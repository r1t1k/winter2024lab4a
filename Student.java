public class Student{
	private String program;
	private String name;
	private int age;
	public int amountLearnt;
	
	public Student(String name, int age){
		this.amountLearnt = 0;
		this.age = age;
		this.name = name;
		this.program = "Computer Science";
	}
	public void sayProgram(){
		System.out.println(this.name + " is in the " + this.program + " Program.");
	}
	
	public void drinkingAge(){
		if(this.age >= 18){
			System.out.println(this.name + " is " + this.age + " years old and can legally drink alcohol.");
		}
		else{
			System.out.println(this.name + " is only " + this.age + " years old and is not allowed to drink alcohol.");
		}
	}
	public void study(int amountStudied){
		this.amountLearnt+= amountStudied;
	}
	
	public int getAge() {
		return this.age;
	}
	public String getProgram() {
		return this.program;
	}
	public String getName() {
		return this.name;
	}
	public int getAmountLearnt() {
		return this.amountLearnt;
	}
	
	public void setProgram(String newProgram) {
		this.program = newProgram; 
	}
}