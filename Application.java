import java.util.Scanner;
public class Application{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Student firstStudent = new Student("Ritik", 17);
		Student secondStudent = new Student("Liam", 18);
		
		System.out.println(firstStudent.getProgram());
		firstStudent.setProgram("Computer Science");
		System.out.println(firstStudent.getProgram());
		
		secondStudent.setProgram("Social Science");

		
		firstStudent.sayProgram();
		secondStudent.sayProgram();
		firstStudent.drinkingAge();
		secondStudent.drinkingAge();
		
		Student[] section4 = new Student[3];
		section4[0] = firstStudent;
		section4[1] = secondStudent;
		section4[2] = new Student("Yanan", 18);
		section4[2].setProgram("Science");

		System.out.println(section4[0].getProgram());
		System.out.println(section4[2].getProgram());
		for(int i = 0; i< section4.length; i++){
			System.out.println(section4[i].getAmountLearnt());
		}
		System.out.println("How many times would you like to study? :");
		int amountStudied = Integer.parseInt(reader.nextLine());
		section4[2].study(amountStudied);
		for(int i = 0; i< section4.length; i++){
			System.out.println(section4[i].getAmountLearnt());
		}
		Student constructorExample = new Student("--Please enter a name--", 18);
		System.out.println(constructorExample.getAmountLearnt());
		System.out.println(constructorExample.getName());
		System.out.println(constructorExample.getProgram());
		System.out.println(constructorExample.getAge());
	}
}